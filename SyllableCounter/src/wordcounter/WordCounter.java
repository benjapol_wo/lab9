package wordcounter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.StringTokenizer;

/**
 * A WordCounter that can find approximate number of words and number of
 * syllables from a text source, or number of syllables in a word.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.3.24
 */
public class WordCounter {

	/**
	 * {@link State} received from the previous character position, initialized
	 * with {@link State#INIT}.
	 */
	private State state = State.INIT;

	/**
	 * Number of syllables from the InputStream in the most recent
	 * {@link #countWords(InputStream)} call.
	 */
	private int recentSyllables = 0;

	/**
	 * Get number of words from an InputStream of text.
	 * 
	 * @param textStream
	 *            - an InputStream that will be counted for words
	 * @return Number of words from the InputStream.
	 */
	public int countWords(InputStream textStream) {
		recentSyllables = 0;
		BufferedReader sourceReader = new BufferedReader(new InputStreamReader(
				textStream));
		int wordCount = 0;
		try {
			String currentLine = sourceReader.readLine();
			StringTokenizer lineToken;
			while (currentLine != null) {
				lineToken = new StringTokenizer(currentLine);
				while (lineToken.hasMoreTokens()) {
					recentSyllables += countSyllables(lineToken.nextToken());
					wordCount++;
				}
				currentLine = sourceReader.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return wordCount;
	}

	/**
	 * Get number of words from a URL pointing to a text source.
	 * 
	 * @param url
	 *            - a URL that will be counted for words
	 * @return Number of words from the URL.
	 */
	public int countWords(URL url) {
		try {
			return countWords(url.openStream());
		} catch (IOException e) {
			e.printStackTrace();
			return 0;
		}
	}

	/**
	 * Get number of all syllables from the InputStream that was recently given
	 * to {@link #countWords(InputStream)}.
	 * 
	 * @return Number of syllables in from the InputStream.
	 */
	public int getSyllableCount() {
		return recentSyllables;
	}

	/**
	 * Get number of syllables from the given word.
	 * 
	 * @param word
	 *            - a word to count for syllables
	 * @return Number of syllables in given word.
	 */
	public int countSyllables(String word) {

		int syllables = 0;

		if (word.startsWith("-") || word.endsWith("-")) {
			state = State.NOT_A_WORD;
		}

		for (char c : word.toCharArray()) {

			if (c == '\'') {
				continue;
			}

			c = Character.toUpperCase(c);

			switch (state) {
			case INIT:
				if (!Character.isLetter(c)) {
					state = State.NOT_A_WORD;
				} else if (c == 'E') {
					state = State.E_FIRST;
				} else if (c == 'A' || c == 'I' || c == 'O' || c == 'U'
						|| c == 'Y') {
					state = State.VOWEL;
				} else {
					state = State.CONSONANT;
				}
				break;
			case CONSONANT:
				if (c == '-') {
					state = State.CONSONANT;
				} else if (!Character.isLetter(c)) {
					state = State.NOT_A_WORD;
				} else if (c == 'E') {
					state = State.E_FIRST;
				} else if (c == 'A' || c == 'I' || c == 'O' || c == 'U'
						|| c == 'Y') {
					state = State.VOWEL;
				} else {
					state = State.CONSONANT;
				}
				break;
			case E_FIRST:
				if (c == '-') {
					state = State.CONSONANT;
				} else if (!Character.isLetter(c)) {
					state = State.NOT_A_WORD;
				} else if (c == 'A' || c == 'E' || c == 'I' || c == 'O'
						|| c == 'U') {
					state = State.VOWEL;
				} else {
					syllables++;
					state = State.CONSONANT;
				}
				break;
			case VOWEL:
				if (c == '-') {
					syllables++;
					state = State.CONSONANT;
				} else if (!Character.isLetter(c)) {
					state = State.NOT_A_WORD;
				} else if (c == 'A' || c == 'E' || c == 'I' || c == 'O'
						|| c == 'U') {
					state = State.VOWEL;
				} else {
					syllables++;
					state = State.CONSONANT;
				}
				break;
			case NOT_A_WORD:
				state = State.INIT;
				return 0;
			}
		}

		if (state == State.VOWEL || (state == State.E_FIRST && syllables == 0)) {
			syllables++;
		}

		state = State.INIT;

		return syllables;
	}

	/**
	 * State(s) of the previous character position, for using with
	 * {@link WordCounter#countSyllables(String)}.
	 * 
	 * @author Benjapol Worakan 5710546577
	 * @version 15.3.24
	 */
	enum State {
		INIT, CONSONANT, E_FIRST, VOWEL, NOT_A_WORD;
	}
}
