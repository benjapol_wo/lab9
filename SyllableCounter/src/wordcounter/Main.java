package wordcounter;

import java.net.MalformedURLException;
import java.net.URL;

import stopwatch.StopWatch;

/**
 * Main class, entry point of the application.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.3.24
 */
public class Main {

	/**
	 * main method, entry point of the application. Will create a new
	 * {@link WordCounter} and {@link StopWatch}, count the syllables and words
	 * in the text file on the Internet, then prints out the counts and time
	 * spent counting.
	 * 
	 * @param args
	 *            - not used
	 */
	public static void main(String[] args) {
		WordCounter wordCounter = new WordCounter();
		StopWatch stopWatch = new StopWatch();
		URL url = null;

		try {
			url = new URL("http://se.cpe.ku.ac.th/dictionary.txt");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		System.out.println("Reading words from " + url.toString());
		System.out.println();

		stopWatch.start();

		int wordCount = wordCounter.countWords(url);
		int syllableCount = wordCounter.getSyllableCount();

		stopWatch.stop();

		System.out.printf("Counted %,d syllables in %,d words\n",
				syllableCount, wordCount);

		System.out.println();
		System.out.printf("Elapsed time %.3f sec\n", stopWatch.getElapsed());
	}
}
