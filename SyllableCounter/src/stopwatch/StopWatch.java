package stopwatch;

/**
 * Basic StopWatch with the ability to calculate elapsed time in nanosecond
 * precision.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.1.29
 */
public class StopWatch {
	/** Constant for converting nanoseconds to seconds. */
	private static final double NANOSECONDS = 1.0E-9;

	/** Container for startTime/endTime. */
	private long startTime, endTime;

	/** Running state of the StopWatch. */
	private boolean running;

	/**
	 * Initialize StopWatch. Set startTime and endTime to current time.
	 */
	public StopWatch() {
		startTime = System.nanoTime();
		endTime = startTime;
	}

	/**
	 * Start the StopWatch, sets startTime to current time, and set running
	 * state to true.
	 * 
	 * This method is effective only when the StopWatch is not already running.
	 */
	public void start() {
		if (!running) {
			startTime = System.nanoTime();
			running = true;
		}
	}

	/**
	 * Stop the StopWatch, sets endTime to current time, and set running state
	 * to false.
	 * 
	 * This method is effective only when the StopWatch is running.
	 */
	public void stop() {
		if (running) {
			endTime = System.nanoTime();
			running = false;
		}
	}

	/**
	 * Get the elapsed time in seconds.
	 * 
	 * If the StopWatch is running, will return the difference between current
	 * time and startTime.
	 * 
	 * If the StopWatch is not running, will return the difference between
	 * startTime and endTime.
	 * 
	 * @return elapsed time in seconds
	 */
	public double getElapsed() {
		double returnCache;
		if (running) {
			returnCache = System.nanoTime() - startTime;
		} else {
			returnCache = endTime - startTime;
		}
		return returnCache * NANOSECONDS;
	}

	/**
	 * Check whether the StopWatch is running or not.
	 * 
	 * @return true if running, false otherwise
	 */
	public boolean isRunning() {
		return running;
	}
}
